#!/usr/bin/env bash

if [ -z "${KUBE_NAMESPACE_PREFIX_BRANCH}" ]
then
    echo KUBE_NAMESPACE_PREFIX_BRANCH is not set.
    exit 1
fi

if [ -z "${KUBE_NAMESPACE_DOMAIN}" ]
then
    echo KUBE_NAMESPACE_DOMAIN is not set.
    exit 1
fi


if [ "${CI_COMMIT_REF_NAME}" = "development" ]
then
    export NAMESPACE=$( echo ${KUBE_NAMESPACE_PREFIX_INTEGRATION}-${KUBE_NAMESPACE_DOMAIN} )
    export NAMESPACE=$( echo ${NAMESPACE} | tr '[:upper:]' '[:lower:]')
else
    export NAMESPACE_B1=$( echo ${KUBE_NAMESPACE_PREFIX_BRANCH}${CI_COMMIT_REF_NAME} ) && export NAMESPACE_B1=${NAMESPACE_B1:0:5}
    export NAMESPACE_B2=${KUBE_NAMESPACE_DOMAIN}
    export NAMESPACE_B3=${CI_COMMIT_REF_SLUG}
    export NAMESPACE=$( echo ${NAMESPACE_B1}-${NAMESPACE_B2}-${NAMESPACE_B3} | tr '[:upper:]' '[:lower:]' )
fi

echo ${NAMESPACE}