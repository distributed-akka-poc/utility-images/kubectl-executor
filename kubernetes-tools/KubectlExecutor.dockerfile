FROM python:2.7.16-alpine3.9

# Build parameters
ARG KUBECTL_LATEST_VERSION=v1.13.4
ARG JQ_VERSION=1.6
#ARG HELM_VERSION=2.13.1
ARG HELM_VERSION=3.0.0
ENV KLV=${KUBECTL_LATEST_VERSION}
ENV JQV=${JQ_VERSION}
ENV HELMV=${HELM_VERSION}

# Fixed value parameters
ENV ALPINE_GLIBC_BASE_URL="https://github.com/sgerrand/alpine-pkg-glibc/releases/download"
ENV ALPINE_GLIBC_PACKAGE_VERSION="2.28-r0"
ENV ALPINE_GLIBC_BASE_PACKAGE_FILENAME="glibc-$ALPINE_GLIBC_PACKAGE_VERSION.apk"
ENV ALPINE_GLIBC_BIN_PACKAGE_FILENAME="glibc-bin-$ALPINE_GLIBC_PACKAGE_VERSION.apk"
ENV ALPINE_GLIBC_I18N_PACKAGE_FILENAME="glibc-i18n-$ALPINE_GLIBC_PACKAGE_VERSION.apk"
ENV AWS_PROFILE=automation-role
ENV INIT_KUBECTL=/usr/local/bin/init-kubectl-aws.sh
ENV CALCULATE_NAMESPACE=/usr/local/bin/calculate-namespace.sh

# Build parameters for envsubst
ENV BUILD_DEPS="gettext"  \
    RUNTIME_DEPS="libintl"

COPY init-kubectl-aws.sh ${INIT_KUBECTL}
COPY calculate-namespace.sh ${CALCULATE_NAMESPACE}
RUN chmod +x ${INIT_KUBECTL} \
    && chmod +x ${CALCULATE_NAMESPACE} \
    && set -x \
    && apk add --no-cache curl \
    && apk add --update $RUNTIME_DEPS \
    && apk add --virtual build_deps $BUILD_DEPS \
    && cp /usr/bin/envsubst /usr/local/bin/envsubst \
    && apk del build_deps \
    # Installation of glibc using alpine packages.
    && apk add --no-cache --virtual=.build-dependencies wget ca-certificates bash \
    && echo \
        "-----BEGIN PUBLIC KEY-----\
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApZ2u1KJKUu/fW4A25y9m\
        y70AGEa/J3Wi5ibNVGNn1gT1r0VfgeWd0pUybS4UmcHdiNzxJPgoWQhV2SSW1JYu\
        tOqKZF5QSN6X937PTUpNBjUvLtTQ1ve1fp39uf/lEXPpFpOPL88LKnDBgbh7wkCp\
        m2KzLVGChf83MS0ShL6G9EQIAUxLm99VpgRjwqTQ/KfzGtpke1wqws4au0Ab4qPY\
        KXvMLSPLUp7cfulWvhmZSegr5AdhNw5KNizPqCJT8ZrGvgHypXyiFvvAH5YRtSsc\
        Zvo9GI2e2MaZyo9/lvb+LbLEJZKEQckqRj4P26gmASrZEPStwc+yqy1ShHLA0j6m\
        1QIDAQAB\
        -----END PUBLIC KEY-----" | sed 's/   */\n/g' > "/etc/apk/keys/sgerrand.rsa.pub" \
    && wget \
        "$ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/$ALPINE_GLIBC_BASE_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/$ALPINE_GLIBC_BIN_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_BASE_URL/$ALPINE_GLIBC_PACKAGE_VERSION/$ALPINE_GLIBC_I18N_PACKAGE_FILENAME" \
    && apk add --no-cache \
        "$ALPINE_GLIBC_BASE_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_BIN_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_I18N_PACKAGE_FILENAME" \
    && rm "/etc/apk/keys/sgerrand.rsa.pub" \
    && /usr/glibc-compat/bin/localedef --force --inputfile POSIX --charmap UTF-8 "$LANG" || true \
    && echo "export LANG=$LANG" > /etc/profile.d/locale.sh \
    && apk del glibc-i18n \
    && rm "/root/.wget-hsts" \
    && apk del .build-dependencies \
    && rm \
        "$ALPINE_GLIBC_BASE_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_BIN_PACKAGE_FILENAME" \
        "$ALPINE_GLIBC_I18N_PACKAGE_FILENAME" \
    && rm -rf /var/cache/apk/* \
    # Set up of tools
    # AWS IAM Authenticator
    && curl -L https://amazon-eks.s3-us-west-2.amazonaws.com/1.11.5/2018-12-06/bin/linux/amd64/aws-iam-authenticator -o /usr/local/bin/aws-iam-authenticator \
    && chmod +x /usr/local/bin/aws-iam-authenticator \
    # Kubectl
    && curl -L https://storage.googleapis.com/kubernetes-release/release/v1.13.4/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    # JSON pocessor
    #&& curl -L https://github.com/stedolan/jq/releases/download/jq-${JQV}/jq-linux64 -o /usr/local/bin/jq \
    && curl -L https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 -o /usr/local/bin/jq \
    && chmod +x /usr/local/bin/jq \
    # Installing Helm
    # The Beta version
    # TODO: Revisit after Helm v3 is officially released
    # TODO: Paramaterize the version
    && curl -L https://get.helm.sh/helm-v3.0.0-linux-amd64.tar.gz -o helm.tar.gz \
    # && curl -L https://storage.googleapis.com/kubernetes-helm/helm-v${HELMV}-linux-amd64.tar.gz -o helm.tar.gz \
    && tar --extract --file=helm.tar.gz --directory /tmp/ \
    && mv /tmp/linux-amd64/helm /usr/local/bin/helm \
    # Installing GCloud CLI
    && curl -O https://dl.google.com/dl/cloudsdk/channels/rapid/downloads/google-cloud-sdk-261.0.0-linux-x86_64.tar.gz \
    && tar zxf google-cloud-sdk-261.0.0-linux-x86_64.tar.gz google-cloud-sdk \
    && export CLOUDSDK_CORE_DISABLE_PROMPTS=1 \
    && ./google-cloud-sdk/install.sh \
    && export PATH=/google-cloud-sdk/bin:$PATH \
    && ln -s /google-cloud-sdk/bin/gcloud /usr/local/bin/gcloud \
    && ln -s /google-cloud-sdk/bin/gsutil /usr/local/bin/gsutil \
    && ln -s /google-cloud-sdk/bin/bq /usr/local/bin/bq \
    && ln -s /google-cloud-sdk/bin/docker-credential-gcloud /usr/local/bin/docker-credential-gcloud \
    && gcloud components update \
    && apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/main --repository  http://dl-cdn.alpinelinux.org/alpine/edge/community docker 

# Hotfix: Suppress it to facilitate Gitlab runner pipeline
# ENTRYPOINT [ "${INIT_KUBECTL}" ]
