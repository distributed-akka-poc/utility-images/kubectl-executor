#!/usr/bin/env bash

echo Initializing prepation

if [ -z "${AWS_ACCESS_KEY_ID}" ]
then
    echo AWS_ACCESS_KEY_ID is not set.
    exit 1
fi

if [ -z "${AWS_SECRET_ACCESS_KEY}" ]
then
    echo AWS_SECRET_ACCESS_KEY is not set.
    exit 1
fi

if [ -z "${AWS_DEFAULT_REGION}" ]
then
    echo AWS_DEFAULT_REGION is not set.
    exit 1
fi

if [ -z "${AWS_ACCESS_ROLE}" ]
then
    echo AWS_ACCESS_ROLE is not set.
    exit 1
fi

if [ -z "${AWS_PROFILE}" ]
then
    echo AWS_PROFILE is not set.
    exit 1
fi


if [ -z "${KUBE_CLUSTER_SERVER}" ]
then
    echo KUBE_CLUSTER_SERVER is not set.
    exit 1
fi

if [ -z "${KUBE_CLUSTER_CERTIFICATE_AUTHORITY_DATA}" ]
then
    echo KUBE_CLUSTER_CERTIFICATE_AUTHORITY_DATA is not set.
    exit 1
fi

if [ -z "${KUBE_CLUSTER_NAME}" ]
then
    echo KUBE_CLUSTER_NAME is not set.
    exit 1
fi


export AWS_CONFIG_CONTENT=$(cat <<AWSCONFIG
[profile ${AWS_PROFILE}]
region=${AWS_DEFAULT_REGION}
[profile ${AWS_PROFILE}-cred]
region=${AWS_DEFAULT_REGION}
AWSCONFIG
)

export AWS_CREDENTIAL_CONTENT=$(cat <<AWSCREDENTIAL
[${AWS_PROFILE}-cred]
aws_access_key_id=${AWS_ACCESS_KEY_ID}
aws_secret_access_key=${AWS_SECRET_ACCESS_KEY}
[${AWS_PROFILE}]
source_profile=${AWS_PROFILE}-cred
role_arn=${AWS_ACCESS_ROLE}
AWSCREDENTIAL
)

export KUBECONFIG_CONTENT=$(cat <<KUBECONFIG
apiVersion: v1
preferences: {}
kind: Config
clusters:
- cluster:
    server: ${KUBE_CLUSTER_SERVER}
    certificate-authority-data: ${KUBE_CLUSTER_CERTIFICATE_AUTHORITY_DATA}
  name: eng-poc-development
contexts:
- context:
    cluster: ${KUBE_CLUSTER_NAME}
    user: aws
    namespace: ${KUBE_NAMESPACE_DOMAIN}
  name: aws
current-context: aws
users:
- name: aws
  user:
    exec:
      apiVersion: client.authentication.k8s.io/v1alpha1
      command: aws-iam-authenticator
      args:
        - "token"
        - "-i"
        - "${KUBE_CLUSTER_NAME}"
      env:
       - name: AWS_PROFILE
         value: ${AWS_PROFILE}
KUBECONFIG
)

# Create AWS configuration files
mkdir ~/.aws
echo -e "${AWS_CONFIG_CONTENT}" > ~/.aws/config
echo -e "${AWS_CREDENTIAL_CONTENT}" > ~/.aws/credentials

# Create kubeconfig file
mkdir ~/.kube
export KUBECONFIG=~/.kube/config
echo -e "${KUBECONFIG_CONTENT}" > ${KUBECONFIG}

export USAGE_MESSAGE=$(cat <<USAGEMESSAGE
#################################################################################################################
You MUST unset the AWS variables by executing the below commands.
Force usage of role declared in the files '~/.aws/credential'.
Without this the 'kubectl' command would not work
$ unset AWS_ACCESS_KEY_ID
$ unset AWS_SECRET_ACCESS_KEY
#################################################################################################################
USAGEMESSAGE
)

echo -e "${USAGE_MESSAGE}"